/**
 * Created by Kenneth on 17/9/2016.
 */
function connect() {


    var WebSocketServer = require('ws').Server;
    var wss = new WebSocketServer({server: server});

    wss.on('connection', function connection(ws) {
        console.log("ws started");
        ws.send('message',function(error) {
            if(error){
                console.error(error);
                global.gc();
            }
        });

        ws.on("close", function () {
            console.log('stopping client interval');
            ws.close();
            ws._socket.destroy();
            global.gc();
        });


        ws.on('error', function(e) {
            console.error('Socket encountered error: ', err.message, 'Closing socket')
        });


        function sendMessageInterval(ws, result){
            setInterval(function(){
                wss.clients.forEach(function each(client) {
                    client.send(JSON.stringify(result), {compress: true}, function (error) {
                        if (error) {
                            console.error(error);
                        }
                    });
                });
            }, 50000);
        }

        ws.on('message', function incoming(message) {
            console.log("Memory heap used --> " + process.memoryUsage().heapUsed);
            var outGoingMessages = [];
            if(!ws.isClosed) {
                client.hgetall("accelerometerData", function (error, result) {
                    if(error){
                        global.gc();
                    }
                    outGoingMessages.push(result);
                });

                client.hgetall("buttonAState", function (error, result) {
                    if(error){
                        global.gc();
                    }
                    outGoingMessages.push(result);
                });

                client.hgetall("buttonBState", function (error, result) {
                    if(error){
                        global.gc();
                    }
                    outGoingMessages.push(result);
                });

                client.hgetall("magnetometerData", function (error, result) {
                    if(error){
                        global.gc();
                    }
                    outGoingMessages.push(result);
                });

                client.hgetall("microbitTemperature", function (error, result) {
                    if(error){
                        global.gc();
                    }
                    outGoingMessages.push(result);
                });
                sendMessageInterval(ws,outGoingMessages);
                global.gc();
            }
        });
    });
}

connect();