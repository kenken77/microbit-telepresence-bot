/**
 * Created by Kenneth on 17/9/2016.
 */
var host = window.document.location.host.replace(/:.*/, '');
var socket = io();
var receivedMessage = false;
var intervals = [];

var g = new JustGage({
    id: "temperatureGauge",
    value: 0,
    min: 0,
    max: 100,
    title: "Temperature °C",
    valueFontColor: "black",
    titleFontColor: "black",
    titleFontFamily: "Courier New, monospace",
    titlePosition: "below"
});

var g2 = new JustGage({
    id: "compassGaugeX",
    value: 0,
    min: -40,
    max: 40,
    title: "Compass X",
    valueFontColor: "black",
    titleFontColor: "black",
    titleFontFamily: "Courier New, monospace",
    titlePosition: "below"
});

var g3 = new JustGage({
    id: "compassGaugeY",
    value: 0,
    min: -40,
    max: 40,
    title: "Compass Y",
    valueFontColor: "black",
    titleFontColor: "black",
    titleFontFamily: "Courier New, monospace",
    titlePosition: "below"
});

var g4 = new JustGage({
    id: "compassGaugeZ",
    value: 0,
    min: -40,
    max: 40,
    title: "Compass Z",
    valueFontColor: "black",
    titleFontColor: "black",
    titleFontFamily: "Courier New, monospace",
    titlePosition: "below"
});

var g5 = new JustGage({
    id: "accelerometerGaugeX",
    value: 0,
    min: -1,
    max: 1,
    title: "accelerometer X",
    valueFontColor: "black",
    titleFontColor: "black",
    titleFontFamily: "Courier New, monospace",
    titlePosition: "below"
});

var g6 = new JustGage({
    id: "accelerometerGaugeY",
    value: 0,
    min: -1,
    max: 1,
    title: "accelerometer Y",
    valueFontColor: "black",
    titleFontColor: "black",
    titleFontFamily: "Courier New, monospace",
    titlePosition: "below"
});

var g7 = new JustGage({
    id: "accelerometerGaugeZ",
    value: 0,
    min: -1,
    max: 1,
    title: "accelerometer Z",
    valueFontColor: "black",
    titleFontColor: "black",
    titleFontFamily: "Courier New, monospace",
    titlePosition: "below"
});


// Whenever the server emits 'login', log the login message
socket.on('login', function (data) {
    connected = true;
});

socket.on('disconnect', function () {
    socket.emit('disconnected');
});


// Whenever the server emits 'new message', update the chat body
socket.on('new message', function (data) {
    var rawData = JSON.stringify(data);
    var rawDataJson = JSON.parse(rawData);
    var x = rawDataJson.message;
    var xx = JSON.parse(x);

    if(xx['sensor-data'].length > 0){
        if(xx['sensor-data'] != null){
            dd = xx['sensor-data'][0];
            if(dd['sensorType'] == 'accelerometer'){
                $('#accelerometer').text(event.data);
                g5.refresh(dd['accelerometerX'] ,1);
                g6.refresh(dd['accelerometerY'] ,1);
                g7.refresh(dd['accelerometerZ'] ,1);
            }
            if(xx['sensor-data'][3] != 'undefined'){
                try {
                    dd3 = xx['sensor-data'][3];
                    if (typeof dd3['sensorType'] != 'undefined' || dd3['sensorType'] == 'buttonA') {
                        //console.log(dd3['buttonAState']);
                        $('#buttonA').html(dd3['buttonAState']);
                    }
                }catch(e){
                    //console.error(e);
                }

            }

            if(xx['sensor-data'][4] != 'undefined') {
                try {
                    dd4 = xx['sensor-data'][4];
                    if (typeof dd4['sensorType'] != 'undefined' || dd4['sensorType'] == 'buttonB') {
                        //console.log(dd4['buttonAState']);
                        $('#buttonB').html(dd4['buttonBState']);
                    }
                }catch(e){
                    //console.error(e);
                }
            }

            if(xx['sensor-data'][5] != 'undefined') {
                try {
                    dd5 = xx['sensor-data'][5];
                    if (typeof dd5['sensorType'] != 'undefined' || dd5['sensorType'] == 'PatientButtonA') {
                        var textOnButtonA = "Not Pressed";
                        if(dd5['buttonAState'] == 'Pressed'){
                            textOnButtonA = "Assistance Require !";
                        }else if(dd5['buttonAState'] == 'Long Press'){
                            textOnButtonA = "EMERGENCY!";
                        }
                        $('#PatientButtonA').html(textOnButtonA);
                    }
                }catch(e){
                   // console.error(e);
                }
            }

            if(xx['sensor-data'][6] != 'undefined') {
                try {
                    dd6 = xx['sensor-data'][6];
                    if (typeof dd6['sensorType'] != 'undefined' || dd6['sensorType'] == 'PatientButtonB') {
                        var textOnButtonB = "Not Pressed";
                        if(dd6['buttonBState'] == 'Pressed'){
                            textOnButtonB = "Replenish Medicine !";
                        }else if(dd6['buttonBState'] == 'Long Press'){
                            textOnButtonB = "Hungry!";
                        }
                        $('#PatientButtonB').html(textOnButtonB);
                    }
                }catch(e){
                    //console.error(e);
                }
            }

            dd2 = xx['sensor-data'][2];
	    if(dd2){	
            	if(dd2['sensorType'] == 'temperature'){
                	$('#temperature').text(event.data);
                	g.refresh(dd2['temperatureValue'] ,100);
            	}
	    }
            dd1 = xx['sensor-data'][1];
            if(dd1['sensorType'] == 'magnetometer'){
                $('#magnetometer').text(event.data);
                g2.refresh(dd1['magnetometerX'] ,40);
                g3.refresh(dd1['magnetometerY'] ,40);
                g4.refresh(dd1['magnetometerZ'] ,40);
            }
        }

    }
    /*var interval_id = setInterval(function(){
        socket.emit('new message', 'done');
    }, 20000);*/
     var interval_id = setInterval(function(){
        socket.emit('new message', 'done');
    }, 1500);
    //console.log(interval_id + ' interval_id');
    intervals.push(interval_id);
    var index = intervals.indexOf(interval_id);
    //console.log(index + ' index');
    if(index >  0){
        //console.log(intervals[index-1] + ' intervals[index-1]');
        clearInterval(intervals[index-1]);
        intervals.splice(index-1, 1);
    }
    //console.log(intervals + ' intervals');
});

// Whenever the server emits 'user joined', log it in the chat body
socket.on('user joined', function (data) {
    console.log(data.username + ' joined');
});

// Whenever the server emits 'user left', log it in the chat body
socket.on('user left', function (data) {
    console.log(data.username + ' left');
});

// Whenever the server emits 'typing', show the typing message
socket.on('typing', function (data) {

});

// Whenever the server emits 'stop typing', kill the typing message
socket.on('stop typing', function (data) {
});

socket.emit('new message', "message");
