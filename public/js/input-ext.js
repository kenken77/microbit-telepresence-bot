/**
 * Created by Kenneth on 17/9/2016.
 */
const HTTPS_PROTOCOL = 'http://';
//const HTTP_PROTOCOL = 'http://';
/*
$("#test").swipe( {
    //Generic swipe handler for all directions
    swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
        if(direction=='up'){
            $.ajax({
                type: 'GET',
                url: HTTPS_PROTOCOL+ window.location.host + '/move-forward',
                success: function(data) {
                }
            });
        }else if(direction='down'){
            $.ajax({
                type: 'GET',
                url: HTTPS_PROTOCOL+ window.location.host + '/move-backward',
                success: function(data) {
                }
            });
        }else if(direction='right'){
            $.ajax({
                type: 'GET',
                url: HTTPS_PROTOCOL+ window.location.host + '/turn-right',
                success: function(data) {
                }
            });
        }else if(direction='left'){
            $.ajax({
                type: 'GET',
                url: HTTPS_PROTOCOL+ window.location.host + '/turn-left',
                success: function(data) {
                }
            });
        }

        $.ajax({
            type: 'GET',
            url: HTTPS_PROTOCOL+ window.location.host + '/stop-motor',
            success: function(data) {
            }
        });
    }
});*/

$(document).keydown(function(e) {
    switch(e.which) {
        case 37: // left
            
            $.ajax({
                type: 'GET',
                url: HTTPS_PROTOCOL+ window.location.host + '/turn-left',
                success: function(data) {
                    $('.blink').html( "<b>LEFT</b>" );
                    $('.blink').blink(); 
                }
            });
            break;

        case 38: // up
            $.ajax({
                type: 'GET',
                url: HTTPS_PROTOCOL+ window.location.host + '/move-forward',
                success: function(data) {
                    $('.blink').html( "<b>FORWARD</b>" );
                    $('.blink').blink(); 
                }
            });
            break;

        case 39: // right
            $.ajax({
                type: 'GET',
                url: HTTPS_PROTOCOL+ window.location.host + '/turn-right',
                success: function(data) {
                    $('.blink').html( "<b>RIGHT</b>" );
                    $('.blink').blink(); 
                }
            });
            break;

        case 40: // down
            $.ajax({
                type: 'GET',
                url: HTTPS_PROTOCOL+ window.location.host + '/move-backward',
                success: function(data) {
                    $('.blink').html( "<b>BACKWARD</b>" );
                    $('.blink').blink(); 
                }
            });
            break;

        case 32: // STOP
            $.ajax({
                type: 'GET',
                url: HTTPS_PROTOCOL+ window.location.host + '/stop-motor',
                success: function(data) {
                    $('.blink').html( "<b>READY</b>" );
                    $('.blink').blink(); 
                }
            });
            break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});

$(document).keyup(function(e) {
    $.ajax({
        type: 'GET',
        url: HTTPS_PROTOCOL+ window.location.host + '/stop-motor',
        success: function(data) {
            $('.blink').html( "" );
        }
    });
});