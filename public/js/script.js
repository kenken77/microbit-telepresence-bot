// Copyright Pololu Corporation.  For more information, see https://www.pololu.com/
stop_motors = true
block_set_motors = false
mouse_dragging = false
const HTTPS_PROTOCOL = 'http://';
function init() {
  //poll()
  $("#joystick").bind("touchstart",touchmove)
  $("#joystick").bind("touchmove",touchmove)
  $("#joystick").bind("touchend",touchend)
  $("#joystick").bind("mousedown",mousedown)
  $(document).bind("mousemove",mousemove)
  $(document).bind("mouseup",mouseup)
}

function poll() {
  $.ajax({url: "status.json"}).done(update_status)
  if(stop_motors && !block_set_motors)
  {
    setMotors(0,0);
    stop_motors = false
  }
}

function update_status(json) {
  s = JSON.parse(json)
  $("#button0").html(s["buttons"][0] ? '1' : '0')
  $("#button1").html(s["buttons"][1] ? '1' : '0')
  $("#button2").html(s["buttons"][2] ? '1' : '0')

  $("#battery_millivolts").html(s["battery_millivolts"])

  $("#analog0").html(s["analog"][0])
  $("#analog1").html(s["analog"][1])
  $("#analog2").html(s["analog"][2])
  $("#analog3").html(s["analog"][3])
  $("#analog4").html(s["analog"][4])
  $("#analog5").html(s["analog"][5])

  setTimeout(poll, 100)
}

function touchmove(e) {
  e.preventDefault()
  touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
  dragTo(touch.pageX, touch.pageY)
}

function resetTouch() {
  stop_motors = true
  //setMotors(0,0);
  $.ajax({
            type: 'GET',
            url: HTTPS_PROTOCOL+ window.location.host + '/stop-motor',
            success: function(data) {
                //setMotorsDone();
            }
        });
}

function mousedown(e) {
  e.preventDefault()
  mouse_dragging = true
}

function mouseup(e) {
  if(mouse_dragging)
  {
    e.preventDefault()
    mouse_dragging = false
    stop_motors = true
  }
}

function mousemove(e) {
  if(mouse_dragging)
  {
    e.preventDefault()
    dragTo(e.pageX, e.pageY)
  }
}

function dragTo(x, y) {
  elm = $('#joystick').offset();
  x = x - elm.left;
  y = y - elm.top;
  w = $('#joystick').width()
  h = $('#joystick').height()

  x = (x-w/2.0)/(w/2.0)
  y = (y-h/2.0)/(h/2.0)

  if(x < -1) x = -1
  if(x > 1) x = 1
  if(y < -1) y = -1
  if(y > 1) y = 1

  left_motor = Math.round(400*(-y+x))
  right_motor = Math.round(400*(-y-x))

  if(left_motor > 400) left_motor = 400
  if(left_motor < -400) left_motor = -400

  if(right_motor > 400) right_motor = 400
  if(right_motor < -400) right_motor = -400

  stop_motors = false
  setMotors(left_motor, right_motor)
}

function touchend(e) {
  console.log("end !");
  e.preventDefault()
  stop_motors = true
}

function moveForward(){
    $.ajax({
          type: 'GET',
          url: HTTPS_PROTOCOL+ window.location.host + '/move-forward',
          success: function(data) {
              console.log("move forward ");
              //setMotorsDone();
          }
      });
}

function moveBackward(){
    $.ajax({
          type: 'GET',
          url: HTTPS_PROTOCOL+ window.location.host + '/move-backward',
          success: function(data) {
              console.log("move back ");
              //setMotorsDone();
          }
      });
}

function turnLeft(){
    $.ajax({
          type: 'GET',
          url: HTTPS_PROTOCOL+ window.location.host + '/turn-left',
          success: function(data) {
              console.log("turn left");
              //setMotorsDone();
          }
      });
}

function turnRight(){
    $.ajax({
          type: 'GET',
          url: HTTPS_PROTOCOL+ window.location.host + '/turn-right',
          success: function(data) {
              console.log("turn right");
              //setMotorsDone();
          }
      });
}

function setMotors(left, right) {
  $("#joystick").html("Motors: " + left + " "+ right)

  if(block_set_motors) return
  block_set_motors = true
  if(left_motor > 0 && right_motor > 0){
      
  }else if(left_motor < 0 && right_motor < 0){
      $.ajax({
            type: 'GET',
            url: HTTPS_PROTOCOL+ window.location.host + '/move-backward',
            success: function(data) {
                console.log("backward");
                setMotorsDone();
            }
        });
  }else if(left_motor < 0 && right_motor > 0){
    $.ajax({
        type: 'GET',
        url: HTTPS_PROTOCOL+ window.location.host + '/turn-left',
        success: function(data) {
            console.log("turn left");
            setMotorsDone();
        }
    });
  }else if(left_motor > 0 && right_motor < 0){
      $.ajax({
            type: 'GET',
            url: HTTPS_PROTOCOL+ window.location.host + '/turn-right',
            success: function(data) {
                setMotorsDone();
            }
        });
  }
  //setMotorsDone();
  //$.ajax({url: "motors/"+left+","+right}).done(setMotorsDone)
}

function setMotorsDone() {
   $.ajax({
            type: 'GET',
            url: HTTPS_PROTOCOL+ window.location.host + '/stop-motor',
            success: function(data) {
                //setMotorsDone();
            }
        });
  block_set_motors = false
}
